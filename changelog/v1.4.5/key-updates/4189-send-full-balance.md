- Add `--fee-included` parameter to `siac wallet send siacoins` that allows
   sending an exact wallet balance with the fees included.