- Fixed file health output of `siac renter -v` not adding to 100% by adding
  parsePercentage function.